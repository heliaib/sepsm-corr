
function output = sEPSMcorr(speech_mix, clean_sig, fs_input)
%
%  sEPSM-corr is an intelligibility prediction model based on the 
%  multi-resolution sEPSM model (Joergensen et al., 2013) and the
%  STOI model (Taal et al., 2011).
%
%  Usage:  sEPSMcorr(speech_mix, clean_sig, fs_input)
%
%   inputs
%   - speech_mix :    mixture of noise + speech (processed or unprocessed)
%   - clean_sig  :    clean speech (unprocessed)
%   - fs_input   :    sample rate  in Hz
%
%   output: a struct with the following fields:
%                 .dfinal    : correlation coefficient between clean and
%                                 processed speech.
%                 .dint      : correlation coefficient matrix for all 
%                                 auditory and modulation filters
%                 .bands     : processed bands (bands above threshold)
%     
%
%  References:
%
%     J�rgensen, S., Ewert, S., and Dau, T. (2013) 'A multi-resolution envelope-power 
%     based model for speech intelligibility', J. Acoust. Soc. Am, 134(1), 436�446.
% 
%     C. H. Taal, R. C. Hendriks, R. Heusdens, and J. Jensen. An Algorithm
%     for Intelligibility Prediction of Time-Frequency Weighted Noisy Speech.
%     IEEE Transactions on Audio, Speech and Language Processing,
%     19(7):2125-2136, 2011.
%
%     Rela�o-Iborra H. et al (2016) submitted to J. Acoust. Soc. Am
%



if nargin < 3
    error('Too few input arguments');
end

if length(speech_mix)~=length(clean_sig)
    error('input signals must have the same length');
end

%% Initialization
speech_mix     = speech_mix(:)';         % Noisy speech row vector
clean_sig      = clean_sig(:)';          % Clean speech row vector

fs             = 22050;                  % Sampling frequency

% Peripheral filters:

cf_aud = [63    80   100  125  160  200    250    315  400 500 ...
     630 800  1000  1250  1600  2000 2500 3150 4000 5000 6300  8000]; % center frequencies of the gammatone filters

% Modulation filters
cf_mod = [1 2 4 8 16 32 64 128 256];% center frequencies of the modulation filters

% Hearing thresholds
HT_diffuse = [37.5 31.5  26.5  22.1 17.9 14.4 11.4 8.4 5.8  3.8 ...
        2.1  1.0  0.8  1.9  0.5 -1.5 -3.1 -4.0 -3.8 -1.8 2.5   6.8 ]; % Diffuse field hearing threshold in quiet: ISO 389-7:2005

if fs_input ~= fs
    speech_mix	= resample(speech_mix, fs, fs_input);
    clean_sig 	= resample(clean_sig, fs, fs_input);
end

N = length(speech_mix);

%% Filtering through gammatone filterbank

g = gammatonefir(cf_aud,fs,'complex');
SN  = 2*real(ufilterbank(speech_mix,g,1));
S   = 2*real(ufilterbank(clean_sig,g,1));

% ---- determining which frequency bands that are above the hearing threshold ---- %

% The spectrum levels (in SPL) of the stimuli are determined from a 1/3-octave analysis, since the threshold to compare with
% is spectrum levels.

mix_rms = thirdOctRMSAnalysis(speech_mix,fs,cf_aud);
mixRMS_dB =  20*log10(mix_rms);
bands = find(mixRMS_dB>HT_diffuse(1:length(cf_aud))); %   The bands to process further are the bands where the mix has spectrum levels above the hearing threshold.

% ---- calculating envelopes of temporal outputs ---- %
%  lowpass filtering at 150 Hz
[bb, aa] = butter(1, 150*2/fs);

sn_env = zeros(N,length(cf_aud));
s_env = zeros(N,length(cf_aud));

sn_env(:,bands) = abs(hilbert(SN(:,bands))); % envelope
s_env(:,bands) = abs(hilbert(S(:,bands)));

sn_env = filter(bb,aa,sn_env);
s_env = filter(bb,aa,s_env);

% downsampling
D = 10;
fsNew = fs/D;

sn_env = resample(sn_env,fsNew,fs);
s_env = resample(s_env,fsNew,fs);


%% Calculating SNRenv

%initialization of correlation matrix
dint_n_p= zeros(length(cf_mod),length(cf_aud));  % no norm, no rect, no thresh


c_thres=0;

for p = bands% For every audio channel
    
      
  % Modulation filterbank analysis
        
    ssnn(:,:,p)  = modFbank_v3(sn_env(:,p),fsNew,cf_mod);
    ss(:,:,p)  = modFbank_v3(s_env(:,p),fsNew,cf_mod);
 
    
    N_ssnn = size(ssnn,2);
    
% Multi-resolution inizialization
    
    WinDurs = 1./cf_mod; %The window duration is the inverse of the centerfrequency of the modulation channel
    
    WinLengths = floor(WinDurs * fsNew);
    Nsegments = ceil(N_ssnn./WinLengths); % The total number of segments is Nframes plus any additional "leftover"
    
    for n = 1:length(cf_mod) %For each modulation channel
       
     %% Multi-resolution time analysis
        
        % Initialize temporary variables:
        tmp_ssnn = zeros(WinLengths(n),Nsegments(n));
        tmp_ss = tmp_ssnn;
    
       % Envelope extraction in the modulation domain
        
        if cf_mod(n) >=10  % In high modulation filters only the 
                           % envelope is processed 
                           
           ssnn(n,:,p)  = abs(hilbert(ssnn(n,:, p)));
           ss(n,:,p)  = abs(hilbert(ss(n,:, p)));
        
           
        end
                
       % Logarithmic compression of the envelopes
        
       sign_ssnn = sign(ssnn(n, :, p)); % Extract sign of signals
       sign_ss = sign(ss(n, :, p));
                     
       ssnn(n, :, p)= 10*log10(max(abs(ssnn(n, :, p)), 0.001)).* sign_ssnn ;
       ss(n, :, p)= 10*log10(max(abs(ss(n, :, p)), 0.001)).* sign_ss ;
   
       segLengths = zeros(1,Nsegments(n)) ;
        
        for i = 1:Nsegments(n) % For each temporal segment of the signal
                               % find the start and end index of the frame
            if i > (Nsegments(n)-1)
                startIdx = 1 + (i-1)*WinLengths(n);
                endIdx = N_ssnn;
            else
                startIdx = 1 + (i-1)*WinLengths(n);
                endIdx = startIdx + WinLengths(n)-1;
            end
            
            segment = startIdx:endIdx;
            segLengths(i) = length(segment);
            
         % internal representation of the temporal segments (samplesPerSegment x number of segments)
         
         tmp_ss(1:segLengths(i),i) = ss(n,segment,p);
         tmp_ssnn(1:segLengths(i),i) = ssnn(n,segment,p);
     
         
    % controling 1 sample segments:
    if startIdx == endIdx
       dint_temp(i, n, p) = 0;
     
    else
      dint_temp(i, n, p) = taa_corr(tmp_ss(1:segLengths(i), i), tmp_ssnn(1:segLengths(i), i));
 
    end
        end

% Rectifying negative values 
dint_temp(dint_temp<0)=0;
  
% Time integration (multiple looks):
dint_n_p(n, p) = sqrt(sum((dint_temp(:, n, p)).^2));


    if cf_mod(n)>0.25*cf_aud(p) % If the modulation freq. is above 0.25 of the 
                                % auditory central frequency, then those channels
                                % do not contribute to intelligibility
      dint_n_p(n, p) = 0;  
      c_thres=c_thres+1;
    end
       
 end
   
end

% Integration of the final correlation index

dfinal = sum(sum(dint_n_p))/(length(bands)*length(cf_mod)-c_thres); 

output.dfinal=dfinal;

output.dint=dint_n_p;

output.bands = bands;

end


function rms_out = thirdOctRMSAnalysis(x,fs,midfreq)

if nargin<3
    midfreq=[63 80 100 125 160 200 250 315 400 500 630 800 1000 1250 1600 2000 2500 3150 4000 5000 6300 8000 ];
end

N = length(x);
X = (fft(x));
X_mag  = abs(X) ;
X_power = X_mag.^2/N ;% power spectrum.
X_power_pos = X_power(1:fix(N/2)+1) ;
X_power_pos(2:end) = X_power_pos(2:end).* (2)  ; %take positive frequencies only and mulitply by two-squared to get the same total energy(used since the integration is only performed for positive freqiencies)

freq= linspace(0,fs/2,length(X_power_pos));

%resolution of data
resol=freq(2)-freq(1);

crossfreq(1)=midfreq(1)/(2^(1/6));
crossfreq(2:length(midfreq)+1)=midfreq*(2^(1/6));

%cross-over indicies
y=crossfreq/resol;

%rounding up
crosselem=round(y);
for n=1:length(y)
    if crosselem(n)<y(n)
        crosselem(n)=crosselem(n)+1;
    end
end

nn=1;
rms_out(1:length(midfreq)) = 0;
while crossfreq(nn+1)<=freq(end)% for nn =1:length(crossfreq)-1
    rms_out(nn) = sqrt(sum(X_power_pos(crosselem(nn):crosselem(nn+1)-1))/N);
    
    nn=nn+1;
    if 1+nn > length(crossfreq)
        break
    end
end

end


function x_filt = modFbank_v3(Env,fs,cf_mod)
%
% This function is an implementation of a modulation filterbank similar to the EPSM-filterbank
% as presented by Ewert & Dau 2000. This implementation consists of a lowpass
% filter with a cutoff at 1 Hz, in parallel with 8 bandpass filters with
% octave spacing. the Center-frequencies of the bandpass filters are lower
% than the original from Ewert & Dau (2000).
%
% Inputs:
%   Env:  The envelope to be filtered
%   fs: sampling frequency of the envelope
%   cf_mod:  centerfrequencies of the modulation filters
%
% Outputs:
%   x_filt:  Temporal outputs for each of the modulation filters
%
%
% Created by S�ren J�rgensen jan 2010
% last update 07 mar 2014
% Copyright S�ren J�rgensen

if nargin<3
    %band center frequencies
    cf_mod=[1 2 4 8 16 32 64 128 256];
end

if size(Env,1) > 1
    Env = Env';
end

if mod(length( Env),2) == 0
    %number is even
    Env =  Env(1:end-1);
else
    %number is odd
end

Q = 1;
N = length(Env);
X = fft(Env);
N_pos_specs = fix(N/2)+1;

pos_freqs= linspace(0,fs/2,N_pos_specs);
freqs = [pos_freqs -1*fliplr(pos_freqs(2:end))];

% Initialize transfer function
TFs = zeros(length(cf_mod),length(freqs));

% Calculating frequency-domain transferfunction for each center frequency:
for k = 2:length(cf_mod)
    TFs(k,1:end) = 1./(1+ (1j*Q*(freqs(1:end)./cf_mod(k) - cf_mod(k)./freqs(1:end)))); % p287 Hambley.
end

fcut = 1;% cutoff frequency of lowpassfilter:
n = 3;% order:
% Lowpass filter squared transfer function:
Wcf(1,:) =  1./(1+((2*pi*freqs/(2*pi*fcut)).^(2*n))); % third order butterworth filter TF from: http://en.wikipedia.org/wiki/Butterworth_filter

TFs(1,:) = sqrt(Wcf(1,:));

X = repmat(X,length(cf_mod),1);
X_filt = X.*TFs;
x_filt = real(ifft(X_filt,N,2));


end

function rho = taa_corr(x, y)
%   RHO = TAA_CORR(X, Y) Returns correlation coeffecient between column
%   vectors x and y. Gives same results as 'corr' from statistics toolbox.
%   See Taal et. al. (2010) & (2011)
xn    	= x-mean(x);
yn   	= y-mean(y);
rho   	= dot(xn/norm(xn),yn/norm(yn));

if isnan(rho)
    rho=0;
end
end