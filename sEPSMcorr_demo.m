%
% Example of use of the sEPSM-corr model.
%
% This script uses the sEPSM-corr model to predict the intelilgibility
% of sentences from the CLUE material mixed with SSN noise, following the 
% experiments described in Nielsen and Dau (2009).
%
%


clear all
close all
clc

%% Initialization
   
Pcorrect_human = [0 8 35 71 90 100 ]; % From Nielsen and Dau 2009

SNRs=-8:2:2; % SNRs tested

 load single_150_SentArray22kHz_varLength % CLUE speech 
   
 Nsentences= 15; % Number of sentences 
 
 sentenceFileLevel = -26; 
 noise_name = 'SSN_CLUE_22kHz.wav'; % SSN Noise
    
 speechSPL = 65;
   
 fs = 22050; % Sampling frequency    
 
 
 fitparam = [-11.87 31.1]; % CLUE fitting values (see sEPSMcorr_CLUE_fitting_example.m for details)
   
 d= zeros(length(SNRs), Nsentences); %Initialization of correlation matrix
    
for q=1:Nsentences
   
    speech  = sentenceArray{q}; %load signal
    speech = speech*10^((speechSPL-sentenceFileLevel)/20); %Adjust level
    N = length(speech);
    
    for n=1:length(SNRs)
  
        noise = audioread(noise_name);
        Nsegments = floor(length(noise)/N);
        startIdx = randi(Nsegments-2 ,1)*N;
        noise = noise(startIdx:startIdx+N -1)'; % random segment from the noise file

        noise = noise./rms(noise)*10^((speechSPL-SNRs(n))/20); % sets the level of the noise signal for the different SNRs

        if size(noise) ~= size(speech)
             noise = noise'; % Flips noise signal if needed
        end

        test = noise + speech; % Generating the noisy speech signal

%         Hybrid model 
        
        tmp = sEPSMcorr(test,speech, fs); 
        d(n, q)= tmp.dfinal; % final correlation-based metric
              
        dint{n, q}= tmp.dint; % save all the across filters correlations 
          
           
    end
 
     disp(q) %Sentence number on screen
end

d_mean= mean(d, 2); % Averaging across sentences


%% Calculation of percentage correct

Pcorrect_hyb = d2pcorrect(d_mean, fitparam);


%% Plotting

xmin = -10;
xmax = 4;
ymin = 0;
ymax = 100;
fnts=12;


figure;

hold all
plot(SNRs, Pcorrect_human, '-o', 'LineWidth', 1.5 )
plot(SNRs, Pcorrect_hyb, '-o', 'LineWidth', 1.5 )

xlabel('SNR [dB]','FontSize',fnts);
ylabel('% correct','FontSize',fnts);
legend('Data', 'sEPSMcorr', 'Location', 'northwest')

ylim([ymin ymax])
xlim([xmin xmax])
set(gca, 'FontSize', fnts)
