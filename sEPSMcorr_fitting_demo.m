
% This scrip finds the free parameters of the logistic function that
% relates the output of the sEPSMcorr model (correlation coefficient d) with the
% percentage of correct answers. The data used for this fitting is taken
% from Nielsen and Dau (2009). The testing conditions are replicated
% and fed to the sEPSMcorr model. Later a least squares analysis is used to fit
% the function to the data.

clear all;
close all;
clc;

%% Initialization

Pcorrect_human = [0 8 35 71 90 100 ]; %% Human data
SNRs= -8:2:2;

load single_150_SentArray22kHz_varLength % CLUE speech 
Nsentences=5;
    
sentenceFileLevel = -26;
noise_name = 'SSN_CLUE_22kHz'; % SSN Noise
speechSPL = 65;

fs = 22050;

d= zeros(length(SNRs), Nsentences);

%% sEPSM-corr
    for q=1:Nsentences
     
    speech  = sentenceArray{q};
    speech = speech*10^((speechSPL-sentenceFileLevel)/20); % Set speech level
    N = length(speech);
    

 for n=1:length(SNRs)
  
  noise = audioread('SSN_CLUE_22kHz.wav');
  Nsegments = floor(length(noise)/N);
  startIdx = randi(Nsegments-2 ,1)*N;
  noise = noise(startIdx:startIdx+N -1)'; % random segment from the noise file

  noise = noise./rms(noise)*10^((speechSPL-SNRs(n))/20); % sets the level of the noise signal

    if size(noise) ~= size(speech)
    noise = noise'; % Flips noise signal if needed
    end

    test = noise + speech; % Gerating the noisy mixture
  
    tmp = sEPSMcorr(speech, test, fs); 
    d(n, q)=tmp.dfinal; % correlation value per sentence and SNR
   
  end
    end
    
d_mean= mean(d, 2); % Model outputs averaged across sentences
    
%% Fitting

xdata = d_mean';
ydata = Pcorrect_human;

fun = @(a,xdata) 100./(1 + exp(a(1)*xdata + a(2))); %Logistic function


pguess = [0 0]; %starting guess
[fit_param,R,J,CovB,MSE] = nlinfit(xdata,ydata,fun,pguess); % non-linear least squares optimization

% Goodness of fit
ysim= fun(fit_param, xdata);
rsq2 = 1 - sum(R.^2) / sum((ydata- mean(ydata)).^2);

%% Plotting

x = linspace(1.5, 3.5, 200);
fit_funct= fun(fit_param, x);

figure
scatter(d_mean, Pcorrect_human, 68, 'filled', 'r')
hold on
plot(x, fit_funct,'k', 'LineWidth', 2)
xlabel('Model output (d)'), ylabel('% correct'), title('sEPSM^{corr} mapping for CLUE material')
le= legend(' Nielsen & Dau (2009) data', 'f_C_L_U_E', 'Location', 'Northwest');
set(le, 'box', 'off')
text(3,12,{[' R^2 = ',num2str(rsq2,2)]},'fontsize',12,'FontName', 'cambria');
set(gca, 'fontsize',12)
