% Get working path
base_path = [fileparts(mfilename('fullpath')),filesep];
external_libraries_path = [base_path,'external',filesep];

% Make sure directory for external libraries exists.
if ~exist([base_path,'external'],'dir'),
	mkdir(base_path,'external');
end

%Add required folders to path
addpath([base_path 'external'])
addpath([base_path, 'demo_signals'])

%% Installing external toolboxes

% The Large Time/Frequency Analysis Toolbox
if(~exist([external_libraries_path,filesep,'ltfat'],'dir')),
   ltfat_path = [external_libraries_path 'ltfat'];
    url = 'http://downloads.sourceforge.net/project/ltfat/ltfat/2.0/ltfat-2.1.2-win64.zip?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fltfat%2F%3Fsource%3Dtyp_redirect&ts=1462204580&use_mirror=freefr';
    if exist(ltfat_path, 'file') == 0,
        files = unzip(url, external_libraries_path);
    end
end

addpath([external_libraries_path 'ltfat'])

% The Spatially Oriented Format for Acoustics 
if(~exist([external_libraries_path,filesep,'sofa'],'dir')),
   sofa_path = [external_libraries_path 'sofa'];
    url = 'http://downloads.sourceforge.net/project/sofacoustics/sofa-api-mo-1.0.1.zip';

    if exist(sofa_path, 'file') == 0,
        files = unzip(url, sofa_path);
    end
end

% addpath(sofa_path)
addpath(genpath( [external_libraries_path 'sofa']))

% Auditory modelling toolbox 
if(~exist([external_libraries_path,filesep,'amtoolbox'],'dir')),
    amtoolbox_path = [external_libraries_path, 'amtoolbox'];
    url = 'http://downloads.sourceforge.net/project/amtoolbox/amtoolbox-0.9.7.zip';
	    if exist(amtoolbox_path, 'file') == 0,
        files = unzip(url,external_libraries_path);
        movefile([external_libraries_path, 'release'], [amtoolbox_path])
	end
end
addpath([external_libraries_path, 'amtoolbox'])

%% Initialize toolboxes
amtstart;
