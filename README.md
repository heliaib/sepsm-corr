# sEPSM-corr model

This repository contains a Matlab implementation of the sEPSM-corr model, as well as examples on how to use it.

## How to copy this repository

### As a repository
Clone the repository in your local machine:

```bash
git clone git@bitbucket.org:heliaib/sepsm-corr.git
```

### Manual Download

Got to the Downloads section of this page, and download the latest released version

##  Getting started

The model requires external toolboxes. They will be installed and added to the working path using:

```bash
run startup
```

Alternatively, you can download the toolboxes and add them to your working directory manually. The required toolboxes are:

+ The Auditory modelling toolbox: http://amtoolbox.sourceforge.net/ which uses:
    - The Large Time-Frequency Analysis Toolbox:  http://ltfat.sourceforge.net/ 
    - Spatially Oriented Format for Acoustics : https://sourceforge.net/projects/sofacoustics/

## Issues

Contact Helia: heliaib@elektro.dtu.dk