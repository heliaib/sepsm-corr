function [Pcorrect] = d2pcorrect(d, fitparam)
% transforms the correlation input (d) into a percentage of correct
% responses (Pcorrect) according to the fitted parameters of a logistic
% function (fitparam)

Pcorrect = 100./(1 + exp(fitparam(1)*d + fitparam(2)));
end